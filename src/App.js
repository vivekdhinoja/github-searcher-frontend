import React, { useState, useEffect } from "react";
import axios from "axios";

import "./App.css";
import Card from "./components/Card";
import Header from "./components/Header";
import Search from "./components/Search";

function App() {
  const options = ["users", "repositories"];
  const [search, setSearch] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [selectedOption, setSelectedOption] = useState(options[0]);

  useEffect(() => {
    const fetchData = (query) => {
      const uri = "http://localhost:8000/api/search/";

      if (query.length >= 3) {
        axios
          .get(uri, {
            params: { q: query, search_type: selectedOption },
          })
          .then((response) => {
            setSearchResults(response.data.items);
          })
          .catch((err) => {
            console.log("error", err);
          });
      } else {
        setSearchResults([]);
      }
    };

    fetchData(search);
  }, [search, selectedOption]);

  return (
    <div className="App">
      <Header />
      <Search
        value={search}
        options={options}
        searchHandler={setSearch}
        selectedOptionValue={selectedOption}
        selectionHandler={setSelectedOption}
        className={!search ? "align-center" : ""}
      />
      <ul className="cards">
        {searchResults.map((item, index) => {
          let user_data = selectedOption === "users" ? item : item.owner;

          return (user_data ? 
            <Card
              key={index}
              profilePicUrl={user_data.avatar_url }
              name={user_data.login}
              profileUrl={user_data.html_url}
              repository_name={selectedOption === "repositories" ? item.name : ""}
              repository_url={selectedOption === "repositories" ? item.html_url : ""}
              forks={selectedOption === "repositories" ? item.forks : ""}
              stars={selectedOption === "repositories" ? item.stargazers_count : ""}
            /> : null
          );
        })}
      </ul>
    </div>
  );
}

export default App;
