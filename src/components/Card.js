import React from 'react'

export default function Card(props) {
  let repository_url = <a href={props.repository_url}>{props.repository_name}</a>
  return (
    <li className="cards_item">
      <div className="card">
        <div className="card_image">
            <img src={props.profilePicUrl} alt="" />
        </div>
        <div className="card_content">
          <h2 className="card_title">
            {props.repository_name ? repository_url : props.name}
          </h2>
          <p className="card_text">{
            props.repository_name ? "Author Name: " + props.name : ""
          }</p>
          <p className="card_text">{
            props.repository_name ? "Forks: " + props.forks : ""
          }</p>
          <p className="card_text">{
            props.repository_name ? "Stars: " + props.stars : ""
          }</p>
          <button 
            className="btn card_btn" 
            onClick={() => window.open(props.profileUrl,'_blank')}>
            View more
            </button>
        </div>
      </div>
    </li>
  )
}
