import React from 'react'

export default function Header(props) {
  let headerClass = "App-header " + props.className;
  return (
    <header className={headerClass}>
      <div>
        <img id="Logo" src='https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png' />
      </div>
      <div className="Header-Text">
          <h1 id="Site-Title">GitHub Searcher</h1>
          <h3 id="Site-Tagline">Search users or repositories below</h3>
        </div>
    </header>
  )
}
