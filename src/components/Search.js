import React from 'react'

export default function Search(props) {
  let searchClass = "Search-form " + props.className;
  return (
    <div className={searchClass}>
        <form action="">
            <input 
              id="search" 
              type="text" 
              value={props.search}
              placeholder="Start typing to search..." 
              onChange={(event) => props.searchHandler(event.target.value)} />
            <select 
              id="search-type"
              style={{'marginLeft': 10}}
              value={props.selectedOptionValue}
              onChange={(event) => props.selectionHandler(event.target.value)}>
              {
                props.options.map((option, index) => {
                  return (
                    <option key={index} value={option}>{option}</option>
                  )
                })
              }
            </select>
        </form>
    </div>
  )
}
